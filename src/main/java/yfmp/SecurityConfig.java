package yfmp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import yfmp.security.RestAuthenticationEntryPoint;
import yfmp.security.RestSavedRequestAwareAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired private RestAuthenticationEntryPoint entryPoint;

	@Autowired private RestSavedRequestAwareAuthenticationSuccessHandler successHandler;
	@Autowired private SimpleUrlAuthenticationFailureHandler failureHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.formLogin()
				.successHandler(successHandler)
				.failureHandler(failureHandler)
				.and()
			.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/users/register", "/login").permitAll()
				.anyRequest().authenticated()
				.and()
			.exceptionHandling()
				.authenticationEntryPoint(entryPoint)
				.and()
			.logout();
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public SimpleUrlAuthenticationFailureHandler getUrlAuthFailureHandler() {
		return new SimpleUrlAuthenticationFailureHandler();
	}
}
