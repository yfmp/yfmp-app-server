package yfmp.heartbeat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class HeartbeatAgent {

	private byte[] buf;
	private DatagramPacket packet;
	private DatagramSocket socket;

	private String host;
	private int port;

	public HeartbeatAgent(int datagramPort, String targetHost, int targetPort) throws SocketException {
		buf = new byte[1];

		socket = new DatagramSocket(datagramPort);
		socket.setSoTimeout(10_000);

		this.host = targetHost;
		this.port = targetPort;
	}

	public void ping() throws IOException {
		buf[0] = (byte) ((buf[0] + 1) % Byte.MAX_VALUE);

		packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(host), port);
		socket.send(packet);
		packet = new DatagramPacket(buf, buf.length);
		socket.receive(packet);

		if(packet.getData()[0] != buf[0])
			throw new RuntimeException("Invalid heartbeat!");
	}

}
