package yfmp.data.hateoas;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import yfmp.service.dto.MetaDto;

@Component
public class HateoasMetaRepository extends HateoasRepository<MetaDto, MetaDto> {

	public HateoasMetaRepository() {
		super("/metas",
				new ParameterizedTypeReference<EntityModel<MetaDto>>() {},
				new ParameterizedTypeReference<CollectionModel<EntityModel<MetaDto>>>() {});
	}

	public Iterable<MetaDto> findByKey(String key) {
		return getAll("/search/findByKey?key=" + key);
	}

	@Override
	protected EntityModel<MetaDto> map(EntityModel<MetaDto> em) {
		em.getContent().setId(
				id(em.getRequiredLink("self").getHref()));

		return em;
	}
}
