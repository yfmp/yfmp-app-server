package yfmp.data.hateoas;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter(AccessLevel.PROTECTED)
@RequiredArgsConstructor
public abstract class HateoasRepository<T, F> {

	@Autowired
	@Getter(AccessLevel.PROTECTED)
	private HateoasDataClient client;

	@Value("${yfmp.data.url}")
	private String dataUrl;

	@NonNull private String prefix;

	@NonNull private ParameterizedTypeReference<EntityModel<T>> ptr;
	@NonNull private ParameterizedTypeReference<CollectionModel<EntityModel<T>>> collectionPtr;

	public Optional<T> add(F obj) {
		return client.post(url(), obj, ptr)
			.map(this::map)
			.map(EntityModel::getContent);
	}

	public Iterable<T> addAll(Iterable<F> objs) {
		return StreamSupport.stream(objs.spliterator(), false)
			.map(this::add)
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toList());
	}

	public Optional<T> get(String path) {
		return client.get(url(path), ptr)
			.map(this::map)
			.map(EntityModel::getContent);
	}

	public Optional<T> getAbsolute(String path) {
		return client.get(path, ptr)
			.map(this::map)
			.map(EntityModel::getContent);
	}

	public Iterable<T> getAll() {
		return getAll(url());
	}

	public Iterable<T> getAllAbsolute(String url) {
		return client.getCollection(url, getCollectionPtr()).getContent().stream()
			.map(this::map)
			.map(EntityModel::getContent)
			.collect(Collectors.toList());
	}

	public Iterable<T> getAll(String path) {
		return client.getCollection(url(path), collectionPtr).getContent().stream()
			.map(this::map)
			.map(EntityModel::getContent)
			.collect(Collectors.toList());
	}

	public void update(String path, T obj) {
		client.put(url(path), obj);
	}

	public void delete(String path) {
		client.delete(url(path));
	}

	public String getUrlWithPrefix() {
		return dataUrl + prefix;
	}

	protected String url(String slug) {
		return String.format("%s%s%s", dataUrl, prefix, slug);
	}

	protected String url() {
		return String.format("%s%s", dataUrl, prefix);
	}

	/**
	 * Translates the URL into object Id
	 *
	 * @param path	URL of the resource
	 * @return ID of the resource to be appended to prefix
	 */
	protected String id(String path) {
		return path.substring(url().length());
	}

	/**
	 * Method that can be overriden in subclasses to perform additional opetaions on EntityModel
	 */
	protected EntityModel<T> map(EntityModel<T> em) {
		return em;
	}
}
