package yfmp.data.hateoas;

import java.util.Optional;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import yfmp.service.dto.PasswordDto;


@Component
public class HateoasPasswordRepository extends HateoasRepository<PasswordDto, PasswordDto> {

	public HateoasPasswordRepository() {
		super("/passwords",
				new ParameterizedTypeReference<EntityModel<PasswordDto>>() {},
				new ParameterizedTypeReference<CollectionModel<EntityModel<PasswordDto>>>() {});
	}

	public Optional<PasswordDto> findByUsername(String name) {
		return get("/search/findByUsername?projection=userWithPassword&name=" + name);
	}

	public Optional<PasswordDto> findByUser(String user) {
		return get("/search/findByUser?projection=userWithPassword&user=" + user);
	}

}
