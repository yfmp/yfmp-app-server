package yfmp.data.hateoas;

import java.util.Optional;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import yfmp.service.dto.UserDto;

@Component
public class HateoasUserRepository extends HateoasRepository<UserDto, UserDto> {

	public HateoasUserRepository() {
		super("/users",
				new ParameterizedTypeReference<EntityModel<UserDto>>() {},
				new ParameterizedTypeReference<CollectionModel<EntityModel<UserDto>>>() {});
	}

	public Optional<UserDto> findByName(String name) {
		return get("/search/findByName?name=" + name);
	}

	public Optional<UserDto> findByEmail(String email) {
		return get("/search/findByEmail?email=" + email);
	}

	@Override
	protected EntityModel<UserDto> map(EntityModel<UserDto> em) {
		em.getContent().setId(
				id(em.getRequiredLink("self").getHref()));

		return em;
	}
}
