package yfmp.data.hateoas;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.Getter;

@Component
public class HateoasDataClient {

	@Autowired
	@Getter
	private RestTemplate rest;

	public <T> Optional<EntityModel<T>> get(String path, ParameterizedTypeReference<EntityModel<T>> ptr) {
		return Optional.of(rest.exchange(path, HttpMethod.GET, null, ptr).getBody());
	}

	public <T> CollectionModel<EntityModel<T>> getCollection(String path, ParameterizedTypeReference<CollectionModel<EntityModel<T>>> ptr) {
		return rest.exchange(path, HttpMethod.GET, null, ptr).getBody();
	}

	public <F,T> Optional<EntityModel<T>> post(String path, F obj, ParameterizedTypeReference<EntityModel<T>> ptr) {
		return Optional.of(rest.exchange(path, HttpMethod.POST, new HttpEntity<F>(obj), ptr).getBody());
	}

	public void delete(String path) {
		rest.delete(path);
	}

	public <F> void put(String path, F obj) {
		rest.put(path, obj);
	}

	public <T, F> Optional<EntityModel<T>> patch(String path, Map<String, F> updates, ParameterizedTypeReference<EntityModel<T>> ptr) {
		return Optional.of(rest.exchange(path, HttpMethod.PATCH,
					new HttpEntity<Map<String, F>>(updates, null), ptr).getBody());
	}

	public <T> Optional<EntityModel<T>> patch(String path, String field, String value, ParameterizedTypeReference<EntityModel<T>> ptr) {
		return patch(path, Collections.singletonMap(field, value), ptr);
	}

	public <T> void postIntoCollection(String path, String objPath) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType("text", "uri-list"));

		HttpEntity<String> req = new HttpEntity<>(objPath, headers);

		rest.exchange(path,
				HttpMethod.POST,
				req,
				String.class);
	}
}
