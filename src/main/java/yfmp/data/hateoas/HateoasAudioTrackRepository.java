package yfmp.data.hateoas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import yfmp.service.dto.AudioTrackDto;
import yfmp.service.dto.CreateAudioTrackDto;

@Component
public class HateoasAudioTrackRepository extends HateoasRepository<AudioTrackDto, CreateAudioTrackDto> {

	@Autowired private HateoasUserRepository users;
	@Autowired private HateoasMetaRepository metas;

	public HateoasAudioTrackRepository() {
		super("/audioTracks",
				new ParameterizedTypeReference<EntityModel<AudioTrackDto>>() {},
				new ParameterizedTypeReference<CollectionModel<EntityModel<AudioTrackDto>>>() {});
	}

	public Iterable<AudioTrackDto> findByOwner(String owner) {
		return getAll("/search/findByOwner?projection=full&owner=" + owner);
	}

	public Iterable<AudioTrackDto> findByMetadataContains(String meta) {
		return getAll("/search/findByMetasContain?projection=full&meta=" + meta);
	}

	@Override
	protected EntityModel<AudioTrackDto> map(EntityModel<AudioTrackDto> em) {
		AudioTrackDto dto = em.getContent();

		dto.setId(id(em.getRequiredLink("self").getHref()));
		dto.setOwner(users.getAbsolute(
					em.getRequiredLink("owner").getHref())
				.orElseThrow());
		dto.setMetadata(metas.getAllAbsolute(
					em.getRequiredLink("metadata").getHref()));

		return em;
	}
}
