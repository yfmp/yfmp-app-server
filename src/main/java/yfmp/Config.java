package yfmp;

import java.io.IOException;
import java.net.SocketException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import yfmp.heartbeat.HeartbeatAgent;

@Configuration
public class Config {

	@Value("${yfmp.data.host}") private String dataHost;
	@Value("${yfmp.data.heartbeatPort}") private int dataHeartbeatPort;
	@Value("${yfmp.app.heartbeatAgentPort}") private int heartbeatPort;

	@Bean
	public HeartbeatAgent getHeartbeatAgent() throws SocketException {
		return new HeartbeatAgent(heartbeatPort, dataHost, dataHeartbeatPort);
	}

	@Value("${trust.store}") private Resource trustStore;
	@Value("${trust.store.password}") private String trustPass;

	@Bean
	public RestTemplate getRestTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
		SSLContext sslContext = new SSLContextBuilder()
			.loadTrustMaterial(trustStore.getURL(), trustPass.toCharArray())
			.build();

		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);

		HttpClient httpClient = HttpClients.custom()
			.setSSLSocketFactory(socketFactory)
			.build();

		HttpComponentsClientHttpRequestFactory factory =
			new HttpComponentsClientHttpRequestFactory(httpClient);

		return new RestTemplate(factory);
	}
}
