package yfmp.service;

import java.util.Optional;

import yfmp.service.dto.MetaDto;

public interface MetaService {

	Iterable<MetaDto> getAlbums();

	Optional<MetaDto> addAlbum(String title);

	Iterable<MetaDto> getArtists();

	Optional<MetaDto> addArtist(String name);

	Optional<MetaDto> addMeta(MetaDto dto);
}
