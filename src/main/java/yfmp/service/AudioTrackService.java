package yfmp.service;

import java.util.Collection;
import java.util.Optional;

import yfmp.service.dto.AudioTrackDto;
import yfmp.service.dto.MetaDto;

public interface AudioTrackService {

	Optional<AudioTrackDto> get(String id);

	Iterable<AudioTrackDto> getForUser(String user);

	Optional<AudioTrackDto> add(MetaDto title, String owner, Collection<String> metadata);

	void remove(String id);
}
