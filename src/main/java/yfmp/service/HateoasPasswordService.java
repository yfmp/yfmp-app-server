package yfmp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import yfmp.data.hateoas.HateoasPasswordRepository;
import yfmp.data.hateoas.HateoasUserRepository;
import yfmp.security.DtoUserDetails;
import yfmp.service.dto.PasswordDto;

@Service
public class HateoasPasswordService implements UserDetailsService {

	@Autowired private HateoasPasswordRepository passwords;
	@Autowired private HateoasUserRepository users;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		return users.findByName(username).flatMap(user ->
				passwords.findByUsername(username)
				.map(PasswordDto::getPassword)
				.map(pass -> new DtoUserDetails(user, pass)))
			.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
	}
}
