package yfmp.service.dto;

import lombok.Data;

@Data
public class CreateTrackDto {

	private String path;

	private String owner;

	private Iterable<String> metadata;
}
