package yfmp.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class MetaDto {

	private String id;

	@NonNull private String key;

	@NonNull private String val;

	}
