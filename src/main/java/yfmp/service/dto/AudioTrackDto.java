package yfmp.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class AudioTrackDto {

	private String id;

	private String path;

	@NonNull private UserDto owner;

	@NonNull private Iterable<MetaDto> metadata;

	}
