package yfmp.service.dto;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class UserDto implements Serializable {

	private String id;

	@NonNull private String name;

	@NonNull private String email;

	}
