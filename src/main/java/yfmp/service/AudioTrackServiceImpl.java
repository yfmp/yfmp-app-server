package yfmp.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import yfmp.data.hateoas.HateoasAudioTrackRepository;
import yfmp.service.dto.AudioTrackDto;
import yfmp.service.dto.CreateAudioTrackDto;
import yfmp.service.dto.MetaDto;

@Component
public class AudioTrackServiceImpl implements AudioTrackService {

	@Autowired private HateoasAudioTrackRepository tracks;

	@Autowired private MetaService metas;

	@Override
	public Optional<AudioTrackDto> get(String id) {
		return tracks.get(id);
	}

	@Override
	public Iterable<AudioTrackDto> getForUser(String user) {
		return tracks.findByOwner(user);
	}

	@Override
	public Optional<AudioTrackDto> add(MetaDto title, String owner, Collection<String> metadata) {

		return metas.addMeta(title)
			.map(MetaDto::getId)
			.map(id -> {
				metadata.add(id);
				return new CreateAudioTrackDto(owner, metadata);
			}).flatMap(tracks::add);
	}

	@Override
	public void remove(String id) {
		tracks.delete(id);
	}
}
