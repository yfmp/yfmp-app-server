package yfmp.service;

import java.util.Optional;

import yfmp.service.dto.CreateUserDto;
import yfmp.service.dto.UserDto;

public interface UserService {

	Optional<UserDto> register(CreateUserDto dto);

}
