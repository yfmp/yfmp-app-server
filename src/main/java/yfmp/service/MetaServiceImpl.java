package yfmp.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import yfmp.data.hateoas.HateoasMetaRepository;
import yfmp.service.dto.MetaDto;

@Component
public class MetaServiceImpl implements MetaService {

	@Autowired
	private HateoasMetaRepository metas;

	@Override
	public Iterable<MetaDto> getAlbums() {
		return metas.findByKey("album");
	}

	@Override
	public Iterable<MetaDto> getArtists() {
		return metas.findByKey("artist");
	}

	@Override
	public Optional<MetaDto> addAlbum(String title) {
		return addMeta(new MetaDto("album", title));
	}

	@Override
	public Optional<MetaDto> addArtist(String name) {
		return addMeta(new MetaDto("artist", name));
	}

	@Override
	public Optional<MetaDto> addMeta(MetaDto dto) {
		return metas.add(dto);
	}

}
