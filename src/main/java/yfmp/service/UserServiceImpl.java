package yfmp.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import yfmp.data.hateoas.HateoasPasswordRepository;
import yfmp.data.hateoas.HateoasUserRepository;
import yfmp.service.dto.CreateUserDto;
import yfmp.service.dto.PasswordDto;
import yfmp.service.dto.UserDto;

@Component
public class UserServiceImpl implements UserService {

	@Autowired private HateoasUserRepository users;
	@Autowired private HateoasPasswordRepository passwords;

	@Autowired private PasswordEncoder encoder;

	@Override
	public Optional<UserDto> register(CreateUserDto dto) {
		// return users.add(dto);
		UserDto user = new UserDto(dto.getName(), dto.getEmail());
		user = users.add(user).orElseThrow();

		PasswordDto pass = new PasswordDto(user.getId(),
				encoder.encode(dto.getPassword()));
		passwords.add(pass);

		return Optional.of(user);
	}
}
