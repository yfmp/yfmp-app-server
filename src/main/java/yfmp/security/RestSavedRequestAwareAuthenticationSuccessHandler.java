package yfmp.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import lombok.Setter;

@Component
public class RestSavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Setter
	private RequestCache reqCache = new HttpSessionRequestCache();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req,
			HttpServletResponse resp,
			Authentication auth) throws ServletException, IOException {

		SavedRequest savedReq = reqCache.getRequest(req, resp);

		if(savedReq == null) {
			clearAuthenticationAttributes(req);
			return;
		}

		String targetUrlParam = getTargetUrlParameter();
		if(isAlwaysUseDefaultTargetUrl()
				|| (targetUrlParam != null
					&& StringUtils.hasText(req.getParameter(targetUrlParam)))) {
			reqCache.removeRequest(req, resp);
			clearAuthenticationAttributes(req);
			return;
					}

		clearAuthenticationAttributes(req);
	}
}
