package yfmp;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;

import yfmp.heartbeat.HeartbeatAgent;

@SpringBootApplication
@EnableAsync
public class Yfmp {

	@Autowired private HeartbeatAgent agent;

	public static void main(String[] args) {
		SpringApplication.run(Yfmp.class, args);
	}

	@Scheduled(fixedDelay = 1000)
	public void pingDataServer() throws IOException {
		agent.ping();
	}

}
