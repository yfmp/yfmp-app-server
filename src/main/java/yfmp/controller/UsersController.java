package yfmp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import yfmp.service.UserService;
import yfmp.service.dto.CreateUserDto;
import yfmp.service.dto.UserDto;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UserService service;

	@PostMapping("/register")
	public UserDto register(@RequestBody CreateUserDto dto) {
		return service.register(dto)
			.orElseThrow();
	}
}
