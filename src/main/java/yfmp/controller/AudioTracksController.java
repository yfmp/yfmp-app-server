package yfmp.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import yfmp.security.DtoUserDetails;
import yfmp.service.AudioTrackService;
import yfmp.service.dto.AudioTrackDto;
import yfmp.service.dto.MetaDto;
import yfmp.service.dto.UserDto;

@RestController
@RequestMapping("/tracks")
public class AudioTracksController {

	@Autowired private AudioTrackService service;

	@PostMapping("/add")
	public AudioTrackDto add(MetaDto title, Collection<String> metadata, UsernamePasswordAuthenticationToken token) {
		return service.add(title,
				getUser(token).getId(), metadata)
			.orElseThrow();
	}

	@DeleteMapping("/remove/{id}")
	public void remove(@PathVariable String id) {
		service.remove(id);
	}

	@GetMapping
	public Iterable<AudioTrackDto> get(UsernamePasswordAuthenticationToken token) {
		return service.getForUser(
				getUser(token).getId());
	}

	@GetMapping("/{id}")
	public ResponseEntity<AudioTrackDto> get(@PathVariable String id) {
		return ResponseEntity.of(service.get("/" + id));
	}

	private UserDto getUser(UsernamePasswordAuthenticationToken token) {
		return ((DtoUserDetails)token.getPrincipal()).getUser();
	}
}
