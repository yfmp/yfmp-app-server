package yfmp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import yfmp.service.MetaService;
import yfmp.service.dto.MetaDto;

@RestController
@RequestMapping("/meta")
public class MetaController {

	@Autowired
	private MetaService service;

	@PostMapping("/add")
	public MetaDto addMeta(String key, String value) {
		return service.addMeta(new MetaDto(key, value))
			.orElseThrow();
	}

	@GetMapping("/albums")
	public Iterable<MetaDto> getAlbums() {
		return service.getAlbums();
	}

	@PostMapping("/albums/add")
	public MetaDto addAlbum(String title) {
		return service.addAlbum(title)
			.orElseThrow();
	}

	@GetMapping("/artists")
	public Iterable<MetaDto> getArtists() {
		return service.getArtists();
	}

	@PostMapping("/artists/add")
	public MetaDto addArtist(String name) {
		return service.addArtist(name)
			.orElseThrow();
	}
}
