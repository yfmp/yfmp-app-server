package yfmp.data.hateoas;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static yfmp.TestUtils.randStr;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import yfmp.service.dto.AudioTrackDto;
import yfmp.service.dto.CreateAudioTrackDto;
import yfmp.service.dto.MetaDto;
import yfmp.service.dto.UserDto;

@SpringBootTest
public class HateoasAudioTrackRepositoryTests {

	@Autowired private HateoasAudioTrackRepository tracks;
	@Autowired private HateoasUserRepository users;
	@Autowired private HateoasMetaRepository metas;

	@Test
	public void testFindByOwner() throws Exception {

		String owner = Optional.of(new UserDto(randStr(), randStr()))
			.flatMap(users::add)
			.map(UserDto::getId)
			.orElseThrow();

		Collection<String> metadata = Optional.of(new MetaDto(randStr(), randStr()))
			.flatMap(metas::add)
			.map(MetaDto::getId)
			.map(Collections::singletonList)
			.orElseThrow();

		CreateAudioTrackDto track = new CreateAudioTrackDto(owner, metadata);

		AudioTrackDto res = tracks.add(track).orElseThrow();

		assertNotNull(res.getId());
		assertEquals(res.getOwner().getId(), track.getOwner());
		assertIterableEquals(
				StreamSupport.stream(res.getMetadata().spliterator(), false)
				.map(MetaDto::getId)
				.collect(Collectors.toList()), track.getMetadata());
	}

	@Test
	public void testFindByOwnerButInOneOptional() {

		Optional.of(new UserDto(randStr(), randStr()))
			.flatMap(users::add)
			.map(UserDto::getId)
			.ifPresent(ownerId -> {
				Optional.of(new MetaDto(randStr(), randStr()))
					.flatMap(metas::add)
					.map(MetaDto::getId)
					.map(Collections::singletonList)
					.map(metadata -> new CreateAudioTrackDto(ownerId, metadata))
					.ifPresent(created -> {
						tracks.add(created)
							.ifPresent(saved -> {
								assertNotNull(saved.getId());
								assertEquals(saved.getOwner().getId(), created.getOwner());
								assertIterableEquals(
										StreamSupport.stream(saved.getMetadata().spliterator(), false)
										.map(MetaDto::getId)
										.collect(Collectors.toList()), created.getMetadata());
							});
					});
			});

	}

	@Test
	public void testFindByMetadataContains() throws Exception {

		String ownerId = Optional.of(new UserDto(randStr(), randStr()))
			.flatMap(users::add)
			.map(UserDto::getId)
			.orElseThrow();

		MetaDto meta = Optional.of(new MetaDto(randStr(), randStr()))
			.flatMap(metas::add)
			.orElseThrow();

		CreateAudioTrackDto create = new CreateAudioTrackDto(ownerId,
				Collections.singleton(meta.getId()));

		Optional.of(create)
			.flatMap(tracks::add)
			.ifPresentOrElse(res -> {

				assertEquals(create.getOwner(), res.getOwner().getId());

				assertIterableEquals(
						StreamSupport.stream(res.getMetadata().spliterator(), false)
						.map(MetaDto::getId)
						.collect(Collectors.toList()), create.getMetadata());

			}, Assertions::fail);
	}
}
