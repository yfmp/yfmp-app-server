package yfmp.data.hateoas;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import yfmp.service.dto.UserDto;

@SpringBootTest
public class HateoasUserRepositoryTests {

	@Autowired
	private HateoasUserRepository repo;

	@Test
	public void testCreate() throws Exception {

		UserDto user = new UserDto("TEST CREATE", "TESTCREATE@example.com");

		UserDto res = repo.add(user)
			.orElseThrow();

		assertEquals(user.getName(), res.getName());
		assertEquals(user.getEmail(), res.getEmail());
	}

	@Test
	public void testFindByName() throws Exception {

		String name = UUID.randomUUID().toString();
		UserDto dto = new UserDto(name, "fbn@example.com");

		dto = repo.add(dto).orElseThrow();

		UserDto res = repo.findByName(name)
			.orElseThrow();

		assertEquals(dto, res);
	}

	@Test
	public void testFindByEmail() throws Exception {

		String email = UUID.randomUUID().toString();
		UserDto dto = new UserDto("TEST FIND BY EMAIL", email);

		dto = repo.add(dto).orElseThrow();

		UserDto res = repo.findByEmail(email)
			.orElseThrow();

		assertEquals(dto, res);
	}
}
