package yfmp.data.hateoas;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static yfmp.TestUtils.randStr;

import java.util.stream.StreamSupport;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import yfmp.service.dto.MetaDto;

@SpringBootTest
public class HateoasMetaRepositoryTests {

	@Autowired
	private HateoasMetaRepository repo;

	@Test
	public void testFindByKey() throws Exception {

		String key = randStr();
		MetaDto dto = new MetaDto(key, randStr());

		dto = repo.add(dto).orElseThrow();

		Iterable<MetaDto> res = repo.findByKey(key);

		assertTrue(StreamSupport.stream(res.spliterator(), false)
				.anyMatch(dto::equals));
	}
}
